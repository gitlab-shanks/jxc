package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.ReturnListGoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    
    @Autowired
    private SaleListGoodsDao saleListGoodsDao;
    
    @Autowired
    private ReturnListGoodsDao returnListGoodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> findListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        int offset = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getListInventory(offset, rows, codeOrName, goodsTypeId);
        
        for (Goods goods : goodsList) {
//            Integer saleTotal= saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId());
//            Integer returnTotal = returnListGoodsDao.getReturnTotalByGoodsId(goods.getGoodsId());
//            if (saleTotal == null) {
//                saleTotal = 0;
//            }
//            if (returnTotal == null) {
//                returnTotal = 0;
//            }
            Integer saleTotal = Optional.ofNullable(saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId())).orElse(0);
            Integer returnTotal = Optional.ofNullable(returnListGoodsDao.getReturnTotalByGoodsId(goods.getGoodsId())).orElse(0);
            goods.setSaleTotal(saleTotal - returnTotal);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("rows", goodsList);
        map.put("total", goodsDao.getInventoryTotal(codeOrName, goodsTypeId));
        return map;
    }
    
}
